#include <zephyr.h>
#include <zephyr/types.h>
#include <stdint.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>

#include <sys/printk.h>
#include <logging/log.h>
LOG_MODULE_REGISTER(BlePeripheral, CONFIG_LOG_MAX_LEVEL); //required to enable LOG_DBG, LOG_ERR
#define sd(x) log_strdup((x))

//Bluetooth advertised name base
#define DEVICE_NAME "NCA-001122334455"

//service UUID and 2 characteristic UUIDs
#define TIDI_SVC_UUID   0x1C, 0xF7, 0x8F, 0x1B, 0x84, 0xBC, 0x85, 0x98, 0x68, 0x49, 0xDA, 0x9E, 0x00, 0x20, 0x0F, 0xF1
#define TIDI_CTRL_UUID  0x1C, 0xF7, 0x8F, 0x1B, 0x84, 0xBC, 0x85, 0x98, 0x68, 0x49, 0xDA, 0x9E, 0x01, 0x20, 0x0F, 0xF1
#define TIDI_INFO_UUID  0x1C, 0xF7, 0x8F, 0x1B, 0x84, 0xBC, 0x85, 0x98, 0x68, 0x49, 0xDA, 0x9E, 0x02, 0x20, 0x0F, 0xF1

//advertising data - Device Info Service & custom service
static const struct bt_data m_ad[] = {
    BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
    BT_DATA_BYTES(BT_DATA_UUID16_ALL, BT_UUID_16_ENCODE(BT_UUID_DIS_VAL)),
    BT_DATA_BYTES(BT_DATA_UUID128_ALL, TIDI_SVC_UUID),
};

static struct bt_conn *m_default_conn = NULL;
static bool m_notify_enabled = false;
static char m_device_name[sizeof(DEVICE_NAME)];

//custom service and characteristics
static struct bt_uuid_128 m_svc_uuid = BT_UUID_INIT_128(TIDI_SVC_UUID);
static const struct bt_uuid_128 m_ctrl_uuid = BT_UUID_INIT_128(TIDI_CTRL_UUID);
static const struct bt_uuid_128 m_info_uuid = BT_UUID_INIT_128(TIDI_INFO_UUID);

static int32_t m_counters[] = {0x0000, 0x8000};
static int32_t m_cmd_val = 0x12345678;

//callback when client reads the command characteristic
static ssize_t read_ctrl_cmd(struct bt_conn *conn, const struct bt_gatt_attr *attr,
			   void *buf, uint16_t len, uint16_t offset)
{
    LOG_DBG("Command read");
	const char *value = attr->user_data;
	return bt_gatt_attr_read(conn, attr, buf, len, offset, value,
				 sizeof(uint32_t));
}

//callback when client writes the command characteristic
ssize_t static write_ctrl_cmd(struct bt_conn *conn, const struct bt_gatt_attr *attr, 
                const void *buf, uint16_t len, uint16_t offset, uint8_t flags) {
    LOG_DBG("Command write: %x", *(uint32_t *)buf);
    m_cmd_val = *(uint32_t *)buf;
    return sizeof(uint32_t);
}

//callback when Client Characteristic Configuration changes
static void counter_ccc_cfg_changed(const struct bt_gatt_attr *attr, uint16_t value) {
	m_notify_enabled = (value == BT_GATT_CCC_NOTIFY) ? true : false;
    LOG_DBG("Notifications %s", (m_notify_enabled ? "enabled" : "disabled"));
}

//custom service
static struct bt_gatt_attr m_attrs[] = {
	BT_GATT_PRIMARY_SERVICE(&m_svc_uuid),
	BT_GATT_CHARACTERISTIC(&m_ctrl_uuid.uuid, 
					BT_GATT_CHRC_READ | BT_GATT_CHRC_WRITE,
			       	BT_GATT_PERM_READ | BT_GATT_PERM_WRITE,
			       	read_ctrl_cmd, write_ctrl_cmd, &m_cmd_val),
    BT_GATT_CHARACTERISTIC(&m_info_uuid.uuid, 
					BT_GATT_CHRC_NOTIFY, BT_GATT_PERM_NONE,
			       	NULL, NULL, NULL),
    BT_GATT_CCC(counter_ccc_cfg_changed, BT_GATT_PERM_READ | BT_GATT_PERM_WRITE),
};
static struct bt_gatt_service tidi_svc = BT_GATT_SERVICE(m_attrs);

//callback on BLE connect
static void connected(struct bt_conn *conn, uint8_t err) {
    if (err) {
        LOG_ERR("Error on BT connect");
    } else {
        LOG_DBG("BT connected...");
        m_default_conn = bt_conn_ref(conn);

        //TODO: pass conn to services that need to write to the client

        const bt_addr_le_t *addr = bt_conn_get_dst(conn);
        LOG_DBG("Peer Address: %x:%x:%x:%x:%x:%x", addr->a.val[0], addr->a.val[1], addr->a.val[2],
            addr->a.val[3], addr->a.val[4], addr->a.val[5]);
    }
}

//callback on BLE disconnect
static void disconnected(struct bt_conn *conn, uint8_t reason) {
    LOG_DBG("BT disconnected...");
    if (m_default_conn != NULL) {

        //TODO: inform clients conn is lost

        bt_conn_unref(m_default_conn);
        m_default_conn = NULL;
    }
}

//struct to register conn/disco callbacks
static struct bt_conn_cb conn_callbacks = {
    .connected = connected,
    .disconnected = disconnected
};

//called on BT stack assert
void bt_ctlr_assert_handle(char *name, int type) {
    if (name != NULL) {
        LOG_ERR("BT Assert-> %s", sd(name));
    }
}

static const char HEX_ASCII_TABLE[] = "0123456789ABCDEF";
static void set_hex_digit(uint8_t digit, uint8_t* dest) {
    *dest++ = HEX_ASCII_TABLE[(digit >> 4) & 0xF];
    *dest = HEX_ASCII_TABLE[digit & 0xF];
}

static void generate_device_name(char *dest) {
    memcpy(dest, DEVICE_NAME, sizeof(DEVICE_NAME));
    
    //read factory information configuration register to get Bluetooth address
    uint8_t *ficr1 = (uint8_t *)0x100000A4;

    //correct endian-ness and set top two bit of address to 11b to meet Bluetooth specification for static address
    uint8_t tmp[6] = {ficr1[5] | 0xC0, ficr1[4], ficr1[3], ficr1[2], ficr1[1], ficr1[0]};
    for (int i = 0; i < 6; i++) {
        set_hex_digit(tmp[i], (dest + i * 2 + 4));
    }
    LOG_DBG("BT Device Name: %s", m_device_name);
}

//callback from BT enable
static void bt_ready(int err) {
    if (err) {
        return;
    }
    generate_device_name(m_device_name);

    //advertised scan data
    const struct bt_data m_sd[] = {
        BT_DATA(BT_DATA_NAME_COMPLETE, m_device_name, sizeof(m_device_name)),
    };

    //register the TIDI service
    bt_gatt_service_register(&tidi_svc);

    //TODO: perform setup of application here

    //begin BLE advertising
    err = bt_le_adv_start(BT_LE_ADV_CONN, m_ad, ARRAY_SIZE(m_ad), m_sd, ARRAY_SIZE(m_sd));
    if (err) {
        LOG_ERR("Adversing failed to start");
        return;
    }
    LOG_DBG("Advertising started...");
}

static void update_counters() {
    m_counters[0] = m_counters[0] + 1;
    m_counters[1] = m_counters[1] + 0x42;
}

void main(void) {
    k_sleep(K_SECONDS(2)); //pause to allow serial monitor to connect to get initial debug

    LOG_DBG("Enabling BLE...");
    int err = bt_enable(bt_ready);
    if (err) {
        LOG_ERR("BLE Enable failed... exiting");
        return;
    }

    //register connect/disconnect callbacks
    bt_conn_cb_register(&conn_callbacks);

    LOG_DBG("Starting main loop...");
    while(1) {
        k_sleep(K_SECONDS(1));

        //update some local values to send in a notification
        update_counters();

        //send notification only when requested by client
        if (m_notify_enabled) {
            bt_gatt_notify(NULL, &m_attrs[3], m_counters, sizeof(m_counters));
        } 
    }
}
